package com.fenech.adrien.pictorius.specialview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.HorizontalScrollView;
import android.widget.ScrollView;

/**
 * Created by Adrien on 10/11/2015.
 */
public class TrackingScrollView extends ScrollView {
    public interface OnScrollChangedListener {
        void onScrollChanged(TrackingScrollView source, int l, int t, int oldl, int oldt);
    }

    public TrackingScrollView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TrackingScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setOnScrollChangedListener(OnScrollChangedListener listener) {
        mOnScrollChangedListener = listener;
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);

        if (mOnScrollChangedListener != null) {
            mOnScrollChangedListener.onScrollChanged(this, l, t, oldl, oldt);
        }
    }

    /** A/D
     *
     */
    private OnScrollChangedListener mOnScrollChangedListener;
}
