package com.fenech.adrien.pictorius.picturemanager;

import android.view.View;
import android.widget.LinearLayout;

import com.fenech.adrien.pictorius.tools.LinkedListSized;

import java.util.LinkedList;
import java.util.UUID;

/**
 * Created by Adrien on 10/11/2015.
 */
public class Album {
    public Album(String title) {
        final int maxSize = 7;
        pics = new LinkedListSized<Pic>(maxSize);
        this.title = title;
        this.upToDate = false;
        this.albumView = null;
    }


    public long getId() {
        return id;
    }

    public LinkedList<Pic> getPics() {
        return pics;
    }

    public String getTitle() {
        return title;
    }

    public boolean isUpToDate() {
        for (Pic pic : pics)
            upToDate &= pic.isUpToDate();
        return this.upToDate;
    }

    public void setAlbumView(View albumView) {
        this.albumView = albumView;
    }

    public void setPictureContainer(LinearLayout pictureContainer) {
        this.pictureContainer = pictureContainer;
    }

    public LinearLayout getPictureContainer() {
        return pictureContainer;
    }

    public boolean isAlbumViewExist() {
        return this.albumView != null;
    }

    /** A/D
     *
     */
    private long id = UUID.randomUUID().getMostSignificantBits();
    private LinkedListSized<Pic> pics;
    private String title;
    private boolean upToDate;
    private View albumView;
    private LinearLayout pictureContainer;
}