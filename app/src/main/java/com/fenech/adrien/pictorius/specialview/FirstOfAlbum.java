package com.fenech.adrien.pictorius.specialview;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.media.Image;
import android.support.v7.widget.LinearLayoutCompat;
import android.text.Layout;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.fenech.adrien.pictorius.R;
import com.fenech.adrien.pictorius.picturemanager.Album;
import com.fenech.adrien.pictorius.tools.BitmapWorkerTask;
import com.fenech.adrien.pictorius.tools.ImageFilters;
import com.fenech.adrien.pictorius.tools.ScreenTools;
import com.fenech.adrien.pictorius.tools.Variables;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import org.w3c.dom.Text;

/**
 * Created by Adrien on 10/11/2015.
 */
public class FirstOfAlbum extends FrameLayout {

    /*public FirstOfAlbum(Context context, Album album, int widthDp) {
        super(context);
        this.album = album;
        this.widthDp = widthDp;
        this.setLayoutParams(
                new LinearLayout.LayoutParams(ScreenTools.dpToPx(context, widthDp), ScreenTools.dpToPx(context, Variables.ALBUM_HEIGHT)));
        this.setBackgroundColor(Color.parseColor("#FFFFFF"));
        ImageView imageView = new ImageView(context);
        imageView.setLayoutParams(
                new LinearLayout.LayoutParams(ScreenTools.dpToPx(context, widthDp), ScreenTools.dpToPx(context, Variables.ALBUM_HEIGHT)));
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
        album.getPics().getFirst().loadBitmap(widthDp, Variables.ALBUM_HEIGHT, imageView, BitmapWorkerTask.NO_FILTER);
        LinearLayout mainLayout = new LinearLayout(context);
        mainLayout.setLayoutParams(
                new LinearLayout.LayoutParams(ScreenTools.dpToPx(context, widthDp), ScreenTools.dpToPx(context, Variables.ALBUM_HEIGHT)));
        mainLayout.setOrientation(LinearLayout.VERTICAL);
        this.addView(imageView);
        this.addView(mainLayout);

        LinearLayout subLayout = new LinearLayout(context);
        subLayout.setLayoutParams(
                new LinearLayout.LayoutParams((int) ScreenTools.dpToPx(context, widthDp), 0, 1) {{ gravity = Gravity.CENTER; }});
        subLayout.setOrientation(LinearLayout.HORIZONTAL);
        subLayout.setGravity(Gravity.CENTER);
        subLayout.setBackgroundColor(Color.parseColor("#40FFFFFF"));

        subLayout.addView(getPersonButton(context));
        subLayout.addView(getLikeButton(context));
        subLayout.addView(getUpgradeButton(context));

        mainLayout.addView(uselessView(context, 2));
        mainLayout.addView(getTitleAlbum(context));
        mainLayout.addView(subLayout);
        mainLayout.addView(uselessView(context, 1));
    }*/

    public FirstOfAlbum(Context context, int widthDp) {
        super(context);
        this.widthDp = widthDp;
        this.setLayoutParams(
                new LinearLayout.LayoutParams(ScreenTools.dpToPx(context, widthDp), ScreenTools.dpToPx(context, Variables.ALBUM_HEIGHT)));
        this.setBackgroundColor(Color.parseColor("#FFFFFF"));

        // Create special view
        createImageView(context);
        createTextView(context);

        // Create mainLayout
        LinearLayout mainLayout = new LinearLayout(context);
        mainLayout.setLayoutParams(
                new LinearLayout.LayoutParams(ScreenTools.dpToPx(context, widthDp), ScreenTools.dpToPx(context, Variables.ALBUM_HEIGHT)));
        mainLayout.setOrientation(LinearLayout.VERTICAL);

        // Create subLayout
        LinearLayout subLayout = new LinearLayout(context);
        subLayout.setLayoutParams(
                new LinearLayout.LayoutParams((int) ScreenTools.dpToPx(context, widthDp), 0, 1) {{
                    gravity = Gravity.CENTER;
                }});
        subLayout.setOrientation(LinearLayout.HORIZONTAL);
        subLayout.setGravity(Gravity.CENTER);
        subLayout.setBackgroundColor(Color.parseColor("#40FFFFFF"));

        // Populate subLayout
        subLayout.addView(getPersonButton(context));
        subLayout.addView(getLikeButton(context));
        subLayout.addView(getUpgradeButton(context));

        // Populate mainLayout
        mainLayout.addView(uselessView(context, 2));
        mainLayout.addView(tv);
        mainLayout.addView(subLayout);
        mainLayout.addView(uselessView(context, 1));

        // Populate frameLayout
        this.addView(imageView);
        this.addView(mainLayout);
    }

    private void createTextView(Context context) {
        tv = new TextView(context);
        tv.setLayoutParams(new LinearLayout.LayoutParams((int) ScreenTools.dpToPx(context, widthDp), 0, 1));
        tv.setGravity(Gravity.CENTER);
        tv.setBackgroundColor(Color.parseColor("#40FFFFFF"));
        tv.setTextSize(ScreenTools.dpToPx(context, 10));
    }
    private void createImageView(Context context) {
        imageView = new ImageView(context);
        imageView.setLayoutParams(
                new LinearLayout.LayoutParams(ScreenTools.dpToPx(context, widthDp), ScreenTools.dpToPx(context, Variables.ALBUM_HEIGHT)));
        imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
    }

    private View uselessView(Context context, final int weight) {
        return new View(context) {{
            setLayoutParams(new LinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 0, weight));
            setBackgroundColor(Color.parseColor("#00000000"));
        }};
    }
    private FloatingActionButton getLikeButton(final Context context) {
        final FloatingActionButton likeButton = new FloatingActionButton(context);
        likeButton.setLayoutParams(new LinearLayoutCompat.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        likeButton.setTitle("I like !");
        likeButton.setIcon(R.mipmap.ic_favorite_white_48dp);
        likeButton.setColorNormal(Color.parseColor("#40000000"));
        likeButton.setColorPressed(Color.parseColor("#40FFFFFF"));
        likeButton.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(context, likeButton.getTitle(), Toast.LENGTH_LONG).show();
                return true;
            }
        });

        likeButton.setOnClickListener(new OnClickListener() {
            boolean isClicked = false;
            @Override
            public void onClick(View v) {
                if (isClicked) {
                    isClicked = false;
                    likeButton.setIcon(R.mipmap.ic_favorite_white_48dp);
                } else {
                    isClicked = true;
                    likeButton.setIcon(R.mipmap.ic_favorite_red_48dp);
                }
            }
        });

        return likeButton;
    }
    private FloatingActionButton getUpgradeButton(final Context context) {
        final FloatingActionButton upgradeButton = new FloatingActionButton(context);
        upgradeButton.setLayoutParams(new LinearLayoutCompat.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        upgradeButton.setTitle("Grade up !");
        upgradeButton.setIcon(R.mipmap.ic_grade_white_48dp);
        upgradeButton.setColorNormal(Color.parseColor("#40000000"));
        upgradeButton.setColorPressed(Color.parseColor("#40FFFFFF"));
        upgradeButton.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(context, upgradeButton.getTitle(), Toast.LENGTH_LONG).show();
                return true;
            }
        });

        upgradeButton.setOnClickListener(new OnClickListener() {
            boolean isClicked = false;

            @Override
            public void onClick(View v) {
                if (isClicked) {
                    isClicked = false;
                    upgradeButton.setIcon(R.mipmap.ic_grade_white_48dp);
                } else {
                    isClicked = true;
                    upgradeButton.setIcon(R.mipmap.ic_grade_yellow_48dp);
                }
            }
        });

        return upgradeButton;
    }
    private FloatingActionButton getPersonButton(final Context context) {
        final FloatingActionButton personButton = new FloatingActionButton(context);
        personButton.setLayoutParams(new LinearLayoutCompat.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT));
        personButton.setTitle("Who is ?");
        personButton.setIcon(R.mipmap.ic_perm_identity_white_48dp);
        personButton.setColorNormal(Color.parseColor("#40000000"));
        personButton.setColorPressed(Color.parseColor("#40FFFFFF"));
        personButton.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Toast.makeText(context, personButton.getTitle(), Toast.LENGTH_LONG).show();
                return true;
            }
        });

        return personButton;
    }

    public void freeFirstOfAlbum() {
        this.imageView.setImageBitmap(null);
        this.tv.setText("");
    }

    public void updateFirstOfAlbum(Album album) {
        album.getPics().getFirst().loadBitmap((int)widthDp, Variables.ALBUM_HEIGHT, imageView, BitmapWorkerTask.NO_FILTER);
        tv.setText(album.getTitle());
    }

    /** A/D
     *
    **/
    private final float widthDp;
    private ImageView imageView;
    private TextView tv;
}
