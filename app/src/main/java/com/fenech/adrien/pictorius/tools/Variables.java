package com.fenech.adrien.pictorius.tools;

import android.graphics.Bitmap;
import android.util.LruCache;

/**
 * Created by Adrien on 11/11/2015.
 */
public class Variables {

    // Be careful to change also the value in the album.xml
    public final static int ALBUM_HEIGHT = 360;
    public final static int MAX_PICTURE_PER_ALBUM = 7;
    public static LruCache<String, Bitmap> mMemoryCache;
}
