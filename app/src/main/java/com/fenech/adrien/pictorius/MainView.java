package com.fenech.adrien.pictorius;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

/**
 * Created by Adrien on 10/11/2015.
 */
public class MainView extends View {
    public MainView(Context context) {
        super(context);
    }

    public MainView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MainView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }
}
