package com.fenech.adrien.pictorius.picturemanager;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import com.fenech.adrien.pictorius.tools.BitmapManager;
import com.fenech.adrien.pictorius.tools.BitmapWorkerTask;
import com.fenech.adrien.pictorius.tools.CacheManager;

import java.lang.ref.WeakReference;
import java.util.Objects;
import java.util.UUID;

/**
 * Created by Adrien on 10/11/2015.
 */
public class Pic {
    public Pic() {
        upToDate = false;
    }

    public Pic(Context context, int resId, String description) {
        this.resId = resId;
        this.context = context;
        this.description = description;
        upToDate = false;
    }

    public Pic(Context context, String path, String description) {
        this.path = path;
        this.context = context;
        this.description = description;
        upToDate = false;
    }

    public Pic(Context context, byte[] byteArray, String description) {
        this.byteArray = byteArray;
        this.context = context;
        this.description = description;
        upToDate = false;
    }

    public String getDescription() {
        return description;
    }

    public long getId() {
        return id;
    }

    public void loadBitmap(int reqWidth, int reqHeight, ImageView imageView) {
        this.loadBitmap(reqWidth, reqHeight, imageView, BitmapWorkerTask.NO_FILTER);
    }

    public void loadBitmap(int reqWidth, int reqHeight, ImageView imageView, int FILTER) {
        String filterKeyWorld = BitmapWorkerTask.getFilterKeyWord(FILTER);
        String reqWidthKeyWorld = String.valueOf(reqWidth);
        String dataKeyWorld = "";

        if (resId != 0)
            dataKeyWorld = String.valueOf(resId);
        else if (!path.equals(""))
            dataKeyWorld = path;
        else
            dataKeyWorld = String.valueOf(byteArray);

        final String imageKey = dataKeyWorld + filterKeyWorld + reqWidthKeyWorld;
        final Bitmap bitmap = CacheManager.getInstance().getBitmapFromMemCache(imageKey);
        if (bitmap != null) {
            imageView.setImageBitmap(bitmap);
        }
        else {
            Bitmap mPlaceHolderBitmap = null;
            if (cancelPotentialWork(resId, imageView)
                    || cancelPotentialWork(path, imageView)
                    || cancelPotentialWork(byteArray, imageView)) {
                final BitmapWorkerTask task = new BitmapWorkerTask(imageView);
                final AsyncDrawable asyncDrawable =
                        new AsyncDrawable(context.getResources(), mPlaceHolderBitmap, task);
                imageView.setImageDrawable(asyncDrawable);

                if (resId != 0)
                    task.execute(context, new Integer(resId), reqWidth, reqHeight, FILTER);
                else if (!path.equals(""))
                    task.execute(context, path, reqWidth, reqHeight, FILTER);
                else
                    task.execute(context, byteArray, reqWidth, reqHeight, FILTER);
            }
        }
        upToDate = true;
    }

    public static boolean cancelPotentialWork(int data, ImageView imageView) {
        final BitmapWorkerTask bitmapWorkerTask = BitmapWorkerTask.getBitmapWorkerTask(imageView);

        if (bitmapWorkerTask != null) {
            final int bitmapData = bitmapWorkerTask.resId;
            // If bitmapData is not yet set or it differs from the new data
            if (bitmapData == 0 || bitmapData != data) {
                // Cancel previous task
                bitmapWorkerTask.cancel(true);
            } else {
                // The same work is already in progress
                return false;
            }
        }
        // No task associated with the ImageView, or an existing task was cancelled
        return true;
    }

    public static boolean cancelPotentialWork(String path, ImageView imageView) {
        final BitmapWorkerTask bitmapWorkerTask = BitmapWorkerTask.getBitmapWorkerTask(imageView);

        if (bitmapWorkerTask != null) {
            final String bitmapData = bitmapWorkerTask.path;
            // If bitmapData is not yet set or it differs from the new data
            if (Objects.equals(bitmapData, "") || bitmapData != path) {
                // Cancel previous task
                bitmapWorkerTask.cancel(true);
            } else {
                // The same work is already in progress
                return false;
            }
        }
        // No task associated with the ImageView, or an existing task was cancelled
        return true;
    }

    public static boolean cancelPotentialWork(byte[] data, ImageView imageView) {
        final BitmapWorkerTask bitmapWorkerTask = BitmapWorkerTask.getBitmapWorkerTask(imageView);

        if (bitmapWorkerTask != null) {
            final byte[] bitmapData = bitmapWorkerTask.byteArray;
            // If bitmapData is not yet set or it differs from the new data
            if (bitmapData.equals(new byte[0]) || bitmapData != data) {
                // Cancel previous task
                bitmapWorkerTask.cancel(true);
            } else {
                // The same work is already in progress
                return false;
            }
        }
        // No task associated with the ImageView, or an existing task was cancelled
        return true;
    }

    public static class AsyncDrawable extends BitmapDrawable {
        private final WeakReference<BitmapWorkerTask> bitmapWorkerTaskReference;

        public AsyncDrawable(Resources res, Bitmap bitmap,
                             BitmapWorkerTask bitmapWorkerTask) {
            super(res, bitmap);
            bitmapWorkerTaskReference =
                    new WeakReference<BitmapWorkerTask>(bitmapWorkerTask);
        }

        public BitmapWorkerTask getBitmapWorkerTask() {
            return bitmapWorkerTaskReference.get();
        }
    }

    public boolean isUpToDate() {
        return upToDate;
    }

    /** A/D
     *
     */
    private long id = UUID.randomUUID().getMostSignificantBits();
    private int resId = 0;
    private String path = "";
    private byte[] byteArray = new byte[0];
    private Context context;
    private String description;
    private boolean upToDate;
}