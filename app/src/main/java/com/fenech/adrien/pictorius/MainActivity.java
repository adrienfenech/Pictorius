package com.fenech.adrien.pictorius;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.LruCache;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.fenech.adrien.pictorius.picturemanager.Album;
import com.fenech.adrien.pictorius.picturemanager.AlbumManager;
import com.fenech.adrien.pictorius.picturemanager.Pic;
import com.fenech.adrien.pictorius.specialview.TrackingScrollView;
import com.fenech.adrien.pictorius.tools.BitmapManager;
import com.fenech.adrien.pictorius.tools.CacheManager;
import com.fenech.adrien.pictorius.tools.ScreenTools;
import com.fenech.adrien.pictorius.tools.Variables;

import java.io.File;
import java.io.IOException;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
        final Context context = this;

        RetainFragment retainFragment =
                RetainFragment.findOrCreateRetainFragment(getFragmentManager());
        CacheManager.getInstance().setMemoryCache(retainFragment.mRetainedCache);
        if (CacheManager.getInstance().getMemoryCache() == null) {
            CacheManager.getInstance().Init();
            retainFragment.mRetainedCache = CacheManager.getInstance().getMemoryCache();
        }
        System.out.println("\033[31m" + CacheManager.getInstance().getMemoryCache().toString() + "\033[0m");


        ((TrackingScrollView) findViewById(R.id.trackingScrollView)).
                setOnScrollChangedListener(new TrackingScrollView.OnScrollChangedListener() {
                    @Override
                    public void onScrollChanged(TrackingScrollView source, int l, int t, int oldl, int oldt) {
                        //handleScroll(source, t);
                    }
                });


        launchViews();
    }

    private void launchViews() {
        LinearLayout container = (LinearLayout) findViewById(R.id.albumContainer);
        LayoutInflater inflater = getLayoutInflater();
        AlbumManager.getInstance().initAlbumManager(this, inflater, container);

        /*Space spacer = new Space(this);
        spacer.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                findViewById(R.id.trackingScrollView).getHeight()));
        container.addView(spacer);*/
        //addMainView(container);
        addNewAlbumView(inflater, container);

        AlbumManager.getInstance().updateViews();
    }

    private void addNewAlbumView(LayoutInflater inflater, LinearLayout albumContainer) {
        final View view = inflater.inflate(R.layout.album, albumContainer, false);

        final LinearLayout pictureContainer = (LinearLayout)view.findViewById(R.id.pictureContainer);

        ImageView ib = new ImageButton(pictureContainer.getContext());
        ib.setLayoutParams(
                new LinearLayout.LayoutParams(
                        ScreenTools.dpToPx(pictureContainer.getContext(), ScreenTools.getScreenDp(this).first),
                        ScreenTools.dpToPx(pictureContainer.getContext(), Variables.ALBUM_HEIGHT)));
        ib.setScaleType(ImageView.ScaleType.CENTER_CROP);
        (new Pic(this, R.mipmap.ic_add_box_white_48dp, "Add Picture")).loadBitmap(ScreenTools.getScreenDp(this).first, Variables.ALBUM_HEIGHT, ib);
        pictureContainer.addView(ib);
        temporaryBitmapManager = new BitmapManager();
        ib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                // Ensure that there's a camera activity to handle the intent
                if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
                    // Create the File where the photo should go
                    File photoFile = null;
                    try {
                        photoFile = temporaryBitmapManager.createImageFile();
                    } catch (IOException ex) {
                        // Error occurred while creating the File
                        ex.printStackTrace();
                    }
                    // Continue only if the File was successfully created
                    if (photoFile != null) {
                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                                Uri.fromFile(photoFile));
                        startActivityForResult(takePictureIntent, REQUEST_TAKE_PHOTO_NEWALBUM);
                    }
                }
            }
        });
        albumContainer.addView(view);
    }

    private void addMainView(LinearLayout container) {
        LinearLayout ll = new LinearLayout(container.getContext());
        ll.setOrientation(LinearLayout.VERTICAL);
        Pair<Integer, Integer> tempPair = ScreenTools.getScreenDp(this);
        ll.setLayoutParams(
                new LinearLayout.LayoutParams(
                        ScreenTools.dpToPx(this, tempPair.first),
                        ScreenTools.dpToPx(this, tempPair.second)));
        ll.setBackgroundColor(Color.parseColor("#88AAFF"));

        container.addView(ll);
    }

    private void handleScroll(ViewGroup source, int top) {
        final float actionBarHeight = getActionBar() != null ? getActionBar().getHeight() : 0f;
        final float firstItemHeight = findViewById(R.id.trackingScrollView).getHeight() - actionBarHeight;
        final float alpha = Math.min(firstItemHeight, Math.max(0, top)) / firstItemHeight;

        mMainView.setTranslationY(-firstItemHeight / 3.0f * alpha);
        if (mActionBarDrawable != null)
            mActionBarDrawable.setAlpha((int) (255 * alpha));

        //View decorView = getWindow().getDecorView();
        //removeOverdraw(decorView, alpha);

        ViewGroup albumContainer = (ViewGroup) source.findViewById(R.id.albumContainer);
        final int count = albumContainer.getChildCount();
        for (int i = 0; i < count; i++) {
            View item = albumContainer.getChildAt(i);

            /*View v = item.findViewById(R.id.state);
            if (v != null && v.getGlobalVisibleRect(mTempRect)) {
                ((StateView) v).reveal(source, item.getBottom());
            }*/
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_TAKE_PHOTO_NEWALBUM && resultCode == RESULT_OK) {
            Album album = new Album("") {{
                getPics().add(new Pic(MainActivity.this, temporaryBitmapManager.getTempPhotoPath(), ""));
            }};
            AlbumManager.getInstance().addNewAlbum(album);
            AlbumManager.getInstance().updateViews();
            //albums.get(2).getPics().get(0).loadBitmap(400, 400, new ImageView(MainActivity.this));
        }
        else if (requestCode == REQUEST_TAKE_PHOTO_ADDPICTURE && resultCode == RESULT_OK) {}
    }

    /** A/D
     *
     */
    private MainView mMainView;
    private Drawable mActionBarDrawable;
    private BitmapManager temporaryBitmapManager;
    private static final int REQUEST_TAKE_PHOTO_NEWALBUM = 1;
    private static final int REQUEST_TAKE_PHOTO_ADDPICTURE = 2;


    static class RetainFragment extends Fragment {
        private static final String TAG = "RetainFragment";
        public LruCache<String, Bitmap> mRetainedCache;

        public RetainFragment() {}

        public static RetainFragment findOrCreateRetainFragment(FragmentManager fm) {
            RetainFragment fragment = (RetainFragment) fm.findFragmentByTag(TAG);
            if (fragment == null) {
                fragment = new RetainFragment();
                fm.beginTransaction().add(fragment, TAG).commit();
            }
            return fragment;
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setRetainInstance(true);
        }
    }
}
