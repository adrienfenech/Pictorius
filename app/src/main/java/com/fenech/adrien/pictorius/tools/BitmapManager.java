package com.fenech.adrien.pictorius.tools;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Adrien on 11/11/2015.
 */
public class BitmapManager {
    public BitmapManager() {

    }

    public Bitmap loadEfficientBitmap(Context context, int resId, int reqWidth, int reqHeight) {
        reqWidth = ScreenTools.dpToPx(context, reqWidth);
        reqHeight = ScreenTools.dpToPx(context, reqHeight);
        final BitmapFactory.Options options = getBitmapOptions(context.getResources(), resId);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(context.getResources(), resId, options);
    }

    public Bitmap loadEfficientBitmap(Context context, String path, int reqWidth, int reqHeight) {
        reqWidth = ScreenTools.dpToPx(context, reqWidth);
        reqHeight = ScreenTools.dpToPx(context, reqHeight);
        final BitmapFactory.Options options = getBitmapOptions(path);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }

    public Bitmap loadEfficientBitmap(Context context, byte[] byteArray, int reqWidth, int reqHeight) {
        reqWidth = ScreenTools.dpToPx(context, reqWidth);
        reqHeight = ScreenTools.dpToPx(context, reqHeight);
        final BitmapFactory.Options options = getBitmapOptions(byteArray);
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);
    }

    private BitmapFactory.Options getBitmapOptions(Resources resources, int resId) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(resources, resId, options);
        //int imageHeight = options.outHeight;
        //int imageWidth = options.outWidth;
        //String imageType = options.outMimeType;
        return options;
    }

    private BitmapFactory.Options getBitmapOptions(String path) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        //int imageHeight = options.outHeight;
        //int imageWidth = options.outWidth;
        //String imageType = options.outMimeType;
        return options;
    }

    private BitmapFactory.Options getBitmapOptions(byte[] byteArray) {
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length, options);
        //int imageHeight = options.outHeight;
        //int imageWidth = options.outWidth;
        //String imageType = options.outMimeType;
        return options;
    }

    private static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        tempPhotoPath = image.getAbsolutePath();
        return image;
    }

    public String getTempPhotoPath() {
        return tempPhotoPath;
    }

    /** A/D
     *
     */
    private String tempPhotoPath;
}
