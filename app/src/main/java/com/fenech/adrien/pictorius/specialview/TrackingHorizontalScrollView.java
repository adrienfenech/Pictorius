package com.fenech.adrien.pictorius.specialview;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.HorizontalScrollView;
import android.widget.ScrollView;

/**
 * Created by Adrien on 10/11/2015.
 */
public class TrackingHorizontalScrollView extends HorizontalScrollView {
    public TrackingHorizontalScrollView(Context context, AttributeSet attrs) {
        super(context, attrs, 0);
    }

    public TrackingHorizontalScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public interface OnScrollChangedListener {
        void onScrollChanged(TrackingHorizontalScrollView source, int l, int t, int oldl, int oldt);
    }

    public void setOnScrollChangedListener(OnScrollChangedListener listener) {
        mOnScrollChangedListener = listener;
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);

        if (mOnScrollChangedListener != null) {
            mOnScrollChangedListener.onScrollChanged(this, l, t, oldl, oldt);
        }
    }

    /** A/D
     *
     */
    private OnScrollChangedListener mOnScrollChangedListener;
}
