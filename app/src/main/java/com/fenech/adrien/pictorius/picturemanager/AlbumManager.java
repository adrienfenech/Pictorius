package com.fenech.adrien.pictorius.picturemanager;

import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fenech.adrien.pictorius.R;
import com.fenech.adrien.pictorius.specialview.FirstOfAlbum;
import com.fenech.adrien.pictorius.specialview.TrackingHorizontalScrollView;
import com.fenech.adrien.pictorius.tools.ScreenTools;
import com.fenech.adrien.pictorius.tools.Variables;

import java.util.ArrayList;

/**
 * Created by Adrien on 16/11/2015.
 */
public class AlbumManager {
    private static AlbumManager ourInstance = new AlbumManager();

    public static AlbumManager getInstance() {
        return ourInstance;
    }

    private AlbumManager() {
    }

    public void initAlbumManager(final Context context, LayoutInflater inflater, LinearLayout albumContainer) {
        this.inflater = inflater;
        this.albumContainer = albumContainer;
        this.context = context;

        albums = new ArrayList<Album>() {{
            add(new Album("Holidays in America") {{
                getPics().add(new Pic(context, R.mipmap.photo_01_antelope, "Light on rock"));
                getPics().add(new Pic(context, R.mipmap.photo_02_tahoe, "\"El mar ~\""));
                getPics().add(new Pic(context, R.mipmap.photo_03_bryce, "The new land"));
            }});
            add(new Album("Another world...") {{
                getPics().add(new Pic(context, R.mipmap.photo_04_powell, "Fishing under stars"));
                getPics().add(new Pic(context, R.mipmap.photo_05_sierra, "Sunrise on mountain"));
            }});
            add(new Album("America II") {{
                getPics().add(new Pic(context, R.mipmap.photo_06_rockaway, "Sunrise on beach"));
                getPics().add(new Pic(context, R.mipmap.photo_07_san_francisco, "Small harbor in San Francisco"));
                getPics().add(new Pic(context, R.mipmap.photo_08_arches, "Arches"));
                getPics().add(new Pic(context, R.mipmap.photo_09_horseshoe, "River"));
                getPics().add(new Pic(context, R.mipmap.photo_10_sky, "Sky above red land"));
            }});
            add(new Album("New home") {{
                getPics().add(new Pic(context, R.mipmap.photo_01_antelope, "Light on rock"));
                getPics().add(new Pic(context, R.mipmap.photo_02_tahoe, "\"El mar ~\""));
                getPics().add(new Pic(context, R.mipmap.photo_03_bryce, "The new land"));
                getPics().add(new Pic(context, R.mipmap.photo_04_powell, "Fishing under stars"));
                getPics().add(new Pic(context, R.mipmap.photo_05_sierra, "Sunrise on mountain"));
            }});
            add(new Album("Another world...") {{
                getPics().add(new Pic(context, R.mipmap.photo_04_powell, "Fishing under stars"));
                getPics().add(new Pic(context, R.mipmap.photo_05_sierra, "Sunrise on mountain"));
            }});
            add(new Album("America II") {{
                getPics().add(new Pic(context, R.mipmap.photo_06_rockaway, "Sunrise on beach"));
                getPics().add(new Pic(context, R.mipmap.photo_07_san_francisco, "Small harbor in San Francisco"));
                getPics().add(new Pic(context, R.mipmap.photo_08_arches, "Arches"));
                getPics().add(new Pic(context, R.mipmap.photo_09_horseshoe, "River"));
                getPics().add(new Pic(context, R.mipmap.photo_10_sky, "Sky above red land"));
            }});
            add(new Album("New home") {{
                getPics().add(new Pic(context, R.mipmap.photo_01_antelope, "Light on rock"));
                getPics().add(new Pic(context, R.mipmap.photo_02_tahoe, "\"El mar ~\""));
                getPics().add(new Pic(context, R.mipmap.photo_03_bryce, "The new land"));
                getPics().add(new Pic(context, R.mipmap.photo_04_powell, "Fishing under stars"));
                getPics().add(new Pic(context, R.mipmap.photo_05_sierra, "Sunrise on mountain"));
            }});
            /*add(new Album() {{
                getPics().add(new Pic(getResources(), R.mipmap.photo_04_powell));
                getPics().add(new Pic(getResources(), R.mipmap.photo_05_sierra));
            }});
            add(new Album() {{
                getPics().add(new Pic(getResources(), R.mipmap.photo_06_rockaway));
                getPics().add(new Pic(getResources(), R.mipmap.photo_07_san_francisco));
                getPics().add(new Pic(getResources(), R.mipmap.photo_08_arches));
                getPics().add(new Pic(getResources(), R.mipmap.photo_09_horseshoe));
                getPics().add(new Pic(getResources(), R.mipmap.photo_10_sky));
                getPics().add(new Pic(getResources(), R.mipmap.photo_06_rockaway));
                getPics().add(new Pic(getResources(), R.mipmap.photo_07_san_francisco));
                getPics().add(new Pic(getResources(), R.mipmap.photo_08_arches));
                getPics().add(new Pic(getResources(), R.mipmap.photo_09_horseshoe));
                getPics().add(new Pic(getResources(), R.mipmap.photo_10_sky));
            }});*/
        }};
    }

    public void updateViews() {
        for (Album album : albums) {
            if (!album.isAlbumViewExist())
                addAlbum(album);
            if (!album.isUpToDate())
                for (Pic pic : album.getPics())
                    if (!pic.isUpToDate())
                        addPicture(album.getPictureContainer(), pic);
        }
    }

    private void addAlbum(final Album album) {
        //final int margin = getResources().getDimensionPixelSize(R.dimen.activity_peek_margin);
        final int margin = 0;
        final View view = inflater.inflate(R.layout.album, albumContainer, false);
        album.setAlbumView(view);

        final LinearLayout pictureContainer = (LinearLayout)view.findViewById(R.id.pictureContainer);
        album.setPictureContainer(pictureContainer);

        pictureContainer.addView(generateFirstOfAlbum(pictureContainer, album));

        ImageView first = null;
        for (Pic pic : album.getPics()) {
            ImageView temp = addPicture(pictureContainer, first, pic);
            first = first == null ? temp : first;
        }

        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0.0f);
        first.setTag(cm);
        first.setColorFilter(new ColorMatrixColorFilter(cm));


        final ImageView bw = first;

        TrackingHorizontalScrollView s =
                (TrackingHorizontalScrollView) view.findViewById(R.id.trackingHorizontaleScrollView);
        s.setOnScrollChangedListener(new TrackingHorizontalScrollView.OnScrollChangedListener() {
            @Override
            public void onScrollChanged(TrackingHorizontalScrollView source,
                                        int l, int t, int oldl, int oldt) {
                final float width = source.getWidth() - margin;
                final float alpha = Math.min(width, Math.max(0, l)) / width;

                if (alpha == 0) {
                    for (int i = 1; i < pictureContainer.getChildCount(); i++)
                        pictureContainer.getChildAt(i).setVisibility(View.INVISIBLE);
                } else {
                    for (int i = 1; i < pictureContainer.getChildCount(); i++)
                        pictureContainer.getChildAt(i).setVisibility(View.VISIBLE);
                }
                if (alpha < 1.0f) {
                    ColorMatrix cm = (ColorMatrix) bw.getTag();
                    cm.setSaturation(alpha);
                    bw.setColorFilter(new ColorMatrixColorFilter(cm));
                } else {
                    bw.setColorFilter(null);
                }
            }
        });
        /*for (int i = pictureContainer.getChildCount() - 1; i > 0; i--)
            pictureContainer.removeViewAt(i);*/

        albumContainer.addView(view);
    }

    private ImageView addPicture(LinearLayout pictureContainer, ImageView first, Pic pic) {
        FrameLayout frame =
                (FrameLayout) inflater.inflate(R.layout.item_picture, pictureContainer, false);
        pic.loadBitmap(ScreenTools.getScreenDp(context).first, Variables.ALBUM_HEIGHT, (ImageView) frame.findViewById(R.id.pic));
        //Bitmap b = pic.loadBitmap(ScreenTools.getScreenDp(this).first, Variables.ALBUM_HEIGHT);
        if (first == null) {
            first = (ImageView)frame.findViewById(R.id.pic);
            //b = ImageFilters.getInstance().applyGaussianBlurEffect(b);
        }
        //((ImageView)frame.findViewById(R.id.pic)).setImageBitmap(b);
        ((TextView)frame.findViewById(R.id.text)).setText(pic.getDescription());
        pictureContainer.addView(frame);
        return first;
    }
    private void addPicture(LinearLayout pictureContainer, Pic pic) {
        FrameLayout frame =
                (FrameLayout) inflater.inflate(R.layout.item_picture, pictureContainer, false);
        pic.loadBitmap(ScreenTools.getScreenDp(context).first, Variables.ALBUM_HEIGHT, (ImageView)frame.findViewById(R.id.pic));
                ((TextView)frame.findViewById(R.id.text)).setText(pic.getDescription());
        pictureContainer.addView(frame);
    }

    public void addNewAlbum(Album newAlbum) {
        this.albums.add(newAlbum);
    }

    private FirstOfAlbum generateFirstOfAlbum(LinearLayout pictureContainer, Album album) {
        return new FirstOfAlbum(
                pictureContainer.getContext(),
                album,
                ScreenTools.getScreenDp(context).first);
    }

    /** A/D
     *
     */
    private ArrayList<Album> albums;
    private Context context;
    private LayoutInflater inflater;
    private LinearLayout albumContainer;
}
