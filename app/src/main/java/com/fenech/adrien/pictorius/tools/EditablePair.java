package com.fenech.adrien.pictorius.tools;

/**
 * Created by Adrien on 20/11/2015.
 */
public class EditablePair<L, R> {
    private final L left;
    private R right;

    public EditablePair(L left, R right) {
        this.left = left;
        this.right = right;
    }

    public L getLeft() {
        return left;
    }

    public R getRight() {
        return right;
    }

    public void setRight(R right) {
        this.right = right;
    }

    @Override
    public int hashCode() {
        return left.hashCode() ^ right.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof EditablePair)) return false;
        EditablePair pairo = (EditablePair) o;
        return this.left.equals(pairo.getLeft()) &&
                this.right.equals(pairo.getRight());
    }

}
