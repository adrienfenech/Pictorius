package com.fenech.adrien.pictorius.picturemanager;

import android.content.Context;
import android.graphics.Picture;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.fenech.adrien.pictorius.R;
import com.fenech.adrien.pictorius.specialview.FirstOfAlbum;
import com.fenech.adrien.pictorius.tools.EditablePair;
import com.fenech.adrien.pictorius.tools.ScreenTools;
import com.fenech.adrien.pictorius.tools.Variables;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.zip.Inflater;

/**
 * Created by Adrien on 20/11/2015.
 */
public class AlbumViewer {
    private static AlbumViewer ourInstance = new AlbumViewer();

    public static AlbumViewer getInstance() {
        return ourInstance;
    }

    private AlbumViewer() {
        this.MAX_ALBUMS = 3;
        this.MAX_PIC_VIEWS = Variables.MAX_PICTURE_PER_ALBUM * this.MAX_ALBUMS;
    }

    private void initAlbumViewMap(Context context, LayoutInflater inflater, LinearLayout albumContainer, int widthDp) {
        this.context = context;
        this.albumContainer = albumContainer;
        albumViewMap = new LinkedHashMap<>();

        for (int albumViewId = 0; albumViewId < MAX_ALBUMS; albumViewId++) {
            FrameLayout albumLayout = (FrameLayout)inflater.inflate(R.layout.album, albumContainer, false);
            FirstOfAlbum firstOfAlbum = new FirstOfAlbum(context, widthDp);
            final LinearLayout pictureContainer = (LinearLayout)albumLayout.findViewById(R.id.pictureContainer);
            pictureContainer.addView(firstOfAlbum);

            FrameLayout[] picViews = new FrameLayout[MAX_PIC_VIEWS];
            for (int picViewId = 0; picViewId < MAX_PIC_VIEWS; picViewId++) {
                FrameLayout frame = (FrameLayout) inflater.inflate(R.layout.item_picture, pictureContainer, false);
                frame.setVisibility(View.GONE);
                pictureContainer.addView(frame);
                picViews[picViewId] = frame;
            }

            EditablePair<FrameLayout, Boolean> frameLayoutBooleanPair = new EditablePair<>(albumLayout, false);
            Pair<FirstOfAlbum, FrameLayout[]> firstOfAlbumPair = new Pair<>(firstOfAlbum, picViews);

            albumViewMap.put(frameLayoutBooleanPair, firstOfAlbumPair);
        }
    }

    public void freeAlbumView(EditablePair<FrameLayout, Boolean> frameLayoutBooleanPair) {
        for (Map.Entry<EditablePair<FrameLayout, Boolean>, Pair<FirstOfAlbum, FrameLayout[]>> entry : albumViewMap.entrySet()) {
            if (entry.getKey().getLeft().equals(frameLayoutBooleanPair.getLeft())) {
                frameLayoutBooleanPair.setRight(false);
                entry.getValue().first.freeFirstOfAlbum();
                FrameLayout[] picViews = entry.getValue().second;
                for (int picViewId = 0; picViewId < this.MAX_PIC_VIEWS; picViewId++) {
                    FrameLayout frame = picViews[picViewId];
                    frame.setVisibility(View.GONE);
                }
                albumContainer.removeView(entry.getKey().getLeft());
            }
        }
    }

    public void updateAlbumView(Album album) {
        for (Map.Entry<EditablePair<FrameLayout, Boolean>, Pair<FirstOfAlbum, FrameLayout[]>> entry : albumViewMap.entrySet()) {
            if (!entry.getKey().getRight()) {
                entry.getValue().first.updateFirstOfAlbum(album);
                FrameLayout[] picViews = entry.getValue().second;
                for (int picViewId = 0; picViewId < album.getPics().size(); picViewId++) {
                    FrameLayout frame = picViews[picViewId];
                    frame.setVisibility(View.VISIBLE);
                    Pic pic = album.getPics().get(picViewId);
                    pic.loadBitmap(ScreenTools.getScreenDp(context).first, Variables.ALBUM_HEIGHT, (ImageView) frame.findViewById(R.id.pic));
                    if (pic.getDescription().length() > 0) {
                        ((TextView) frame.findViewById(R.id.text)).setText(album.getPics().get(picViewId).getDescription());
                        ((TextView) frame.findViewById(R.id.text)).setVisibility(View.VISIBLE);
                    } else
                        ((TextView) frame.findViewById(R.id.text)).setVisibility(View.VISIBLE);
                }
                albumContainer.addView(entry.getKey().getLeft());
                entry.getKey().setRight(true);
            }
        }
    }

    /**A/D
     *
     */
    private LinkedHashMap<EditablePair<FrameLayout, Boolean>, Pair<FirstOfAlbum, FrameLayout[]>> albumViewMap;
    private LinearLayout albumContainer;
    private Context context;

    private final int MAX_PIC_VIEWS;
    private final int MAX_ALBUMS;
}
