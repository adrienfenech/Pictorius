package com.fenech.adrien.pictorius.tools;

import java.util.LinkedList;

/**
 * Created by Adrien on 10/11/2015.
 */
public class LinkedListSized<T> extends LinkedList<T> {
    public LinkedListSized(int maxSize) {
        super();
        this.maxSize = maxSize;
    }

    @Override
    public boolean add(T obj) {
        if (size() < maxSize)
            return super.add(obj);
        return false;
    }

    @Override
    public T set(int id, T obj) {
        if (id < maxSize)
            return super.set(id, obj);
        return obj;
    }


    /** A/D
     *
     */
    private final int maxSize;
}
