package com.fenech.adrien.pictorius.tools;

import android.graphics.Bitmap;
import android.util.LruCache;

/**
 * Created by Adrien on 11/11/2015.
 */
public class CacheManager {
    private static CacheManager ourInstance = new CacheManager();

    public static CacheManager getInstance() {
        return ourInstance;
    }

    private CacheManager() {
        Init();
    }

    public void Init() {
        // Get max available VM memory, exceeding this amount will throw an
        // OutOfMemory exception. Stored in kilobytes as LruCache takes an
        // int in its constructor.
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);

        // Use 1/8th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 2;

        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

    public LruCache<String, Bitmap> getMemoryCache() {
        return mMemoryCache;
    }

    public void setMemoryCache(LruCache<String, Bitmap> mMemoryCache) {
        this.mMemoryCache = mMemoryCache;
    }

    /** A/D
     *
     */
    private LruCache<String, Bitmap> mMemoryCache;
}
