package com.fenech.adrien.pictorius.tools;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.widget.ImageView;

import com.fenech.adrien.pictorius.picturemanager.Pic;

import java.lang.ref.WeakReference;

/**
 * Created by Adrien on 11/11/2015.
 */
public class BitmapWorkerTask extends AsyncTask<Object, Void, Bitmap> {

    private final WeakReference<ImageView> imageViewReference;
    public Integer resId = 0;
    public String path = "";
    public byte[] byteArray = new byte[0];

    public BitmapWorkerTask(ImageView imageView) {
        // Use a WeakReference to ensure the ImageView can be garbage collected
        imageViewReference = new WeakReference<ImageView>(imageView);
    }

    // Decode image in background.
    @Override
    protected Bitmap doInBackground(Object... params) {
        Context context = (Context)params[0];
        int reqWidth = (int)params[2];
        int reqHeight = (int)params[3];

        Bitmap bitmap;
        if (params[1] instanceof String) {
            path = (String) params[1];
            bitmap = new BitmapManager().loadEfficientBitmap(context, path, reqWidth, reqHeight);
        }
        else if (params[1] instanceof Integer) {
            resId = (Integer) params[1];
            bitmap = new BitmapManager().loadEfficientBitmap(context, resId, reqWidth, reqHeight);
        }
        else {
            byteArray = (byte[]) params[1];
            bitmap = new BitmapManager().loadEfficientBitmap(context, byteArray, reqWidth, reqHeight);
        }

        int FILTER = -1;
        if (params.length >= 5 && params[4] != null)
            FILTER = (int)params[4];
        String filterKeyWorld = getFilterKeyWord(FILTER);
        String reqWidthKeyWorld = String.valueOf(reqWidth);
        switch (FILTER) {
            case FILTER_FASTBLUR:
                bitmap = ImageFilters.getInstance().applyFirstOfAlbumEffect(bitmap, 20, 1000);
                break;
            default: break;
        }
        if (resId != 0)
            CacheManager.getInstance().addBitmapToMemoryCache(String.valueOf(resId) + filterKeyWorld + String.valueOf(reqWidthKeyWorld), bitmap);
        else if (!path.equals(""))
            CacheManager.getInstance().addBitmapToMemoryCache(path + filterKeyWorld + String.valueOf(reqWidthKeyWorld), bitmap);
        else if (!byteArray.equals(new byte[0]))
            CacheManager.getInstance().addBitmapToMemoryCache(String.valueOf(byteArray) + filterKeyWorld + String.valueOf(reqWidthKeyWorld), bitmap);
        return bitmap;
    }

    // Once complete, see if ImageView is still around and set bitmap.
    @Override
    protected void onPostExecute(Bitmap bitmap) {
        if (isCancelled()) {
            bitmap = null;
        }

        if (imageViewReference != null && bitmap != null) {
            final ImageView imageView = imageViewReference.get();
            final BitmapWorkerTask bitmapWorkerTask =
                    getBitmapWorkerTask(imageView);
            if (this == bitmapWorkerTask && imageView != null) {
                imageView.setImageBitmap(bitmap);
            }
        }
    }

    public static BitmapWorkerTask getBitmapWorkerTask(ImageView imageView) {
        if (imageView != null) {
            final Drawable drawable = imageView.getDrawable();
            if (drawable instanceof Pic.AsyncDrawable) {
                final Pic.AsyncDrawable asyncDrawable = (Pic.AsyncDrawable) drawable;
                return asyncDrawable.getBitmapWorkerTask();
            }
        }
        return null;
    }

    public static String getFilterKeyWord(int FILTER) {
        switch (FILTER) {
            case FILTER_FASTBLUR: return KEY_FILTER_FASTBLUR;
            default: return KEY_NO_FILTER;
        }
    }
    public static final int NO_FILTER = 0;
    public static final String KEY_NO_FILTER = "NO_FILTER";
    public static final int FILTER_FASTBLUR = 1;
    public static final String KEY_FILTER_FASTBLUR = "FILTER_FASTBLUR";
}
