package com.fenech.adrien.pictorius.tools;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Pair;

/**
 * Created by Adrien on 11/11/2015.
 */
public class ScreenTools {
    private static Pair<Integer, Integer> screenDp = null;
    public static Pair<Integer, Integer> getScreenDp(Context context) {
        //if (screenDp != null)
            //return screenDp;
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();

        float dpHeight = displayMetrics.heightPixels / displayMetrics.density;
        float dpWidth = displayMetrics.widthPixels / displayMetrics.density;
        screenDp = new Pair<>((int)dpWidth, (int)dpHeight);
        return screenDp;
    }

    public static int dpToPx(Context context, float dp) {
        return (int)(context.getResources().getDisplayMetrics().density * dp + 0.5f);
    }
}
