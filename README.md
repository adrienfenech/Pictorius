# Pictorius

## Summary

Pictorius is an Android app to share picture with friend or anyone.
The main challenge here is to deal with large number of picture in high quality using ImageView and effect.
This app is base on the project of RomainGuy : Road-Trip (https://github.com/romainguy/road-trip). I keep the hortizontal and vertical scroll views and their architecture.

For some reasons, and because the main purpose of this app is to experiment bitmap usage, this app deals with album (unlimited) and picture (maximum of 7 pictures by album). Why only 7 pictures ? Because during a trip, only few pictures are really wonderful !

## Main features

Pictorius uses some nice features :
  * Load optimizations (DONE)
  * Display optimizations (TODO)
  * Memory caching (DONE)
  * Picture effect (FIX TODO) *--> Issue on FastBlurEffect*
  * Upload and download of content (TODO)
  * Friend manager
